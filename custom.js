// Shows and hides the menu on mobile
function mobileMenu() {
    document.getElementById("menu-links").classList.toggle("menu-open");
}

document.getElementById("menu-seach-icon").addEventListener("click", searchMenu);
// Opens and closes the menu search bar
function searchMenu() {
    var menuSearch = document.getElementById("menu-search");
    var menuSearchIcon = document.getElementById("menu-seach-icon");

    if (menuSearch.classList.contains("menu-search-open")) {
        menuSearch.classList.remove("menu-search-open");
        menuSearchIcon.addEventListener("click", searchMenu);
        menuSearchIcon.removeEventListener("click", search);
    } else {
        menuSearch.classList.add("menu-search-open");
        menuSearchIcon.removeEventListener("click", searchMenu);
        menuSearchIcon.addEventListener("click", search);
    }
}

// Shows and hides the login popup
function loginPopup() {
    document.getElementById("login-popup").classList.toggle("login-popup-open");
}

// Searches for a product
function search() {
    const form = document.getElementById('menu-search');
    form.submit();
};

// Enlarges the clicked on image
$('.productImages').on('click', (elm) => {
    const src = elm.target.src;
    const alt = elm.target.alt;

    var bigImage = document.getElementById("bigImage");
    bigImage.src = src;
    bigImage.alt = alt;
});

// Shows and hides the notification popup
function notification ($message, $error) {
    let elem = createNotification($message, $error);
    let time = 4000;
    setTimeout(() => {
        elem.classList.add("show");
    }, 10)
    setTimeout(() => {
        elem.classList.remove("show");
    }, time);
    setTimeout(() => {
        elem.remove();
    }, time + 500)
}

//generate notification
function createNotification ($message, $error) {
    if ($error){
        //change text color if it an error
        $error = 'text-danger';
    }
    let notification = $('<div id="notification"><h6 class="'+$error+'">'+$message+'</h6></div>');
    $('body').append(notification);
    elem = document.getElementById("notification");
    return elem;
}

// Prevents buttons from submitting forms
$('.prevent').on("click",function(e){
    e.preventDefault();
});

// Opens other delever address popup
function openForm(){
    let form = document.getElementById('deliverAdressForm');
    form.classList.add("showForm");
}

// Closes other delever address popup
function closeForm(){
    let form = document.getElementById('deliverAdressForm');
    form.classList.remove("showForm");
}

// Submits the form
function submitForm (elm) {
    elm.submit();
}

// Adds the cadeau wrapping costs to the total costs
function calc (elm) {
    let tp = $('#tp');
    let tpex = $('#tpex');
    let html;
    if($(elm).prop('checked')){
        let html = tp.html();
        html = Number(html.replace('€ ' ,'')) + 1.99;
        tp.html('€ '+html.toFixed(2));
        html = tpex.html();
        html = Number(html.replace('€ ' ,'')) + (1.99 * 0.79);
        tpex.html('€ '+html.toFixed(2));
    }
    else {
        let html = tp.html();
        html = Number(html.replace('€ ' ,'')) - 1.99;
        tp.html('€ '+html.toFixed(2));
        html = tpex.html()
        html = Number(html.replace('€ ' ,'')) - (1.99 * 0.79);
        tpex.html('€ '+html.toFixed(2));
    }
}
