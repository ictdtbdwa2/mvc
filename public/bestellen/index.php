<?php
    include_once('../core.php');

    $deliverhtml = '';
    $deliverformhtml = '';
    $tpex = 0;
    $tp = 0;
    
    // Adds the address to user data when a new delivery address is added
    if (isset($_POST['newAddress'])){
        $userinformation = $_POST;
        $_GET['deliveraddress'] = addAddress($_SESSION['user_id'], $userinformation, '0');
    }

    // Redirects to the shoppingbasket if the shoppingbasket is empty
    if (empty($_SESSION['shopping_cart'])){
        echo '
            <script>
                location.replace("/winkelwagen");
            </script>
        ';
    }
    
    $profile = Select("select * from wideworldimporters.v_Choc_profilepage where customerid = ".$_SESSION["user_id"]." limit 1")[0];
    $deliverAddresses = Select("select * from wideworldimporters.v_Choc_customerdeliveryaddresses where customerid = ".$_SESSION["user_id"]);
    $defaultDeliverAddresses = Select("select * from wideworldimporters.v_Choc_customerdeliveryaddresses where customerid = ".$_SESSION["user_id"]." and deliveryaddressdefault = 1")[0];
    
    // If no deliveraddress is present set the default deliveraddress from the database
    if(!isset($_GET['deliveraddress']) || empty($_GET['deliveraddress'])){
        $_GET['deliveraddress'] = $defaultDeliverAddresses['deliveryaddressesid'];
    }

    // Gets all the delivery addresses and shows them in the delivery address popup
    foreach($deliverAddresses as $deliverAddress) {
        if ($deliverAddress['deliveryaddressesid'] == $_GET['deliveraddress']){
            $deliverhtml .= '
            <div class="col-4">
                <div class="deliver-address">
                    <p>'.$profile["customername"].'<br>
                    '.$deliverAddress['addressline1'].'<br>
                    '.$deliverAddress['postalcode'].' '.strtoupper($deliverAddress['city']).'</p>
                </div>
            </div>';
            $deliverformhtml .= '
            <div class="col-6">
                <form onclick="submitForm(this)" class="border border-success rounded m-2 p-2">
                    <input type="hidden" name="deliveraddress" value="'.$deliverAddress['deliveryaddressesid'].'">
                    <div>'.$profile["customername"].'</div>
                    <div>'.$deliverAddress['addressline1'].'</div>
                    <div>'.$deliverAddress['postalcode'].' '.strtoupper($deliverAddress['city']).'</div>
                </form>
            </div>';
        } else {
            $deliverformhtml .= '
            <div class="col-6">
                <form onclick="submitForm(this)" class="border border-info rounded m-2 p-2">
                    <input type="hidden" name="deliveraddress" value="'.$deliverAddress['deliveryaddressesid'].'">
                    <div>'.$profile["customername"].'</div>
                    <div>'.$deliverAddress['addressline1'].'</div>
                    <div>'.$deliverAddress['postalcode'].' '.strtoupper($deliverAddress['city']).'</div>
                </form>
            </div>';
        }
    }

    // Calculates the total price of all products
    if($products){
        foreach($products as $product){
            $tpex += number_format($product['unitprice'] * $product['amount'], 2);
            $tp += number_format(number_format($product['unitprice'] * 1.21,2) * $product['amount'], 2);
        }
    }
?>

<!DOCTYPE html>
<html lang="nl">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Chocoly</title>
    <link href="<?=$link?>images/favicon.svg" rel="icon" >
    <link href="<?=$link?>css/main.css" rel="stylesheet">
    <script src="<?=$link?>js/jquery.min.js"></script>
</head>
<body>
    <?php include($link."menu.php"); ?>
    <div id="bestellen" class="container">
        <form action="/betalen" method="get">
            <div class="row">
                <div class="col-6">
                    <h3 class="header">Bestellen</h3>
                    <div class="form-section">
                        <label>Bezorgadres</label>
                        <div class='section-small'>
                            <div class="row">
                                <?=$deliverhtml?>
                            </div>
                        </div>
                        <button onclick="openForm()" class="prevent btn btn-dark">Ander bezorgadres</button>
                    </div>
                    <div class="form-section">
                        <input onclick="calc(this)" type="checkbox" name="isGift">
                        <label>Cadeau verpakking?</label>
                        <b class="cadeau-cost">(€ 1,99)</b>
                    </div>
                    <div class="form-section">
                        <label>Bezorgadres</label>
                        <input required class='d-block my-2' type="date" min='<?=date('Y-m-d',strtotime('+1 day'))?>' name="date" id="">
                    </div>
                    <div class="form-section">
                        <label>Betaalmethode</label>
                        <input checked type="radio" name="payment">
                        <label>IDeal</label>
                    </div>
                    <div>
                        <input type="hidden" required value="<?=$_GET['deliveraddress']?>" name="deliveradress">
                        <input type="submit" class="btn btn-dark" value="Afrekenen">
                    </div>
                </div>
                <div class="col-2"></div>
                <div class="col-4">
                    <div class="bg-info cart-price section-small">
                        <div class="section-small">
                            <b class="text-white w-50">Totale prijs excl btw:</b><p id="tpex" class="text-white w-50 text-right">€ <?=number_format($tpex, 2);?></p>
                        </div>
                        <div class="section-small">
                            <b class="text-white w-50">BTW:</b><p class="text-white w-50 text-right">21%</p>
                        </div>
                        <div class="divider-top">
                            <b class="text-white w-50">Totale prijs:</b><p id="tp" class="text-white w-50 text-right">€ <?=number_format($tp, 2)?></p>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <div id="deliverAdressForm">
        <div class="p-5">
            <h5>Kies een adres</h5>
            <div class="row my-3">
                <?=$deliverformhtml?>
            </div>
            <form action="" method="post">
                <h5 class="my-3"> Nieuw adres toevoegen</h5>
                <div class="row my-3">
                    <div class="col-9">
                        <b>Straat</b>
                        <input required name="street" class="d-block w-100" type="text">
                    </div>
                    <div class="col-3">
                        <b>Nr.</b>
                        <input required name="housenumber" class="d-block w-100" type="text">
                    </div>
                    <div class="col-4">
                        <b>Postcode</b>
                        <input placeholder="8123AB" name="postalcode" pattern="^([0-9]{4})([A-Z]{2})?$" required class="d-block w-100" type="text">
                    </div>
                    <div class="col-8">
                        <b>Woonplaats</b>
                        <input required name="city" class="d-block w-100" type="text">
                    </div>
                </div>
                <div class="row">
                    <div class="col-6 text-left">
                        <button onclick="closeForm();" class="btn btn-dark prevent">Sluiten</button>
                    </div>
                    <div class="col-6 text-right">
                        <input type="submit" name="newAddress" class="btn btn-primary" value="Toevoegen">
                    </div>
                </div>
            </form>
           
        </div>
    </div>
</div>
    <?php include_once($link.'footer.php'); isAuth();?>
</body>
</html>