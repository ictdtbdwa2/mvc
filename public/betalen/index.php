<?php
    include_once('../core.php');
    $tp = isset($_GET['isGift']) ? (1.99 / 121 * 100)  : 0;

    // Redirects to the shoppingbasket if the shoppingbasket is empty
    if(empty($_SESSION['shopping_cart'])){
        echo '
            <script>
                location.replace("/winkelwagen");
            </script>
        ';
    }
    
    // If the order is payed a invoice is added in the database
    if(isset($_GET)){
        if (!empty($_GET['betaald'])){
            addInvoice();
            unset($_SESSION['settings']);
        }
        else {
            $_SESSION['settings'] = $_GET;
        }
    }

    // Calculates the total price of all products
    if($products){
        foreach($products as $product){
            $tp += number_format(number_format($product['unitprice'] * 1.21, 2) * $product['amount'], 2);
        }
    }
?>

<style>
* {
  box-sizing: border-box;
}
 
html,
body {
  font-family: 'Montserrat', sans-serif;
  display: flex;
  width: 100%;
  height: 100%;
  background: #f4f4f4;
  justify-content: center;
  align-items: center;
}
.checkout-panel {
  display: flex;
  flex-direction: column;
  width: 940px;
  height: 766px;
  background-color: rgb(255, 255, 255);
  box-shadow: 0 1px 1px 0 rgba(0, 0, 0, .2);
}
.panel-body {
  padding: 45px 80px 0;
  flex: 1;
}
 
.title {
  font-weight: 700;
  margin-top: 0;
  margin-bottom: 40px;
  color: #2e2e2e;
}

</style>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Chocoly | betalen</title>
    <link href="<?=$link?>images/favicon.svg" rel="icon" >
    <link href="<?=$link?>css/main.css" rel="stylesheet">
</head>
<body>
    <div class="checkout-panel">
        <div class="panel-body">
            <div class="row">
                <div class="col-6">
                    <h2 class="title my-2"><?= isset($_GET['betaald']) ? "Betaald" : "Betalen via IDeal" ?></h2>
                
                    <?php 
                        // As long as the order isn't payed the price of the order is shown
                        if (!isset($_GET['betaald'])){
                            echo '
                                <h1 class="my-5">
                                    € '.number_format($tp, 2).'
                                </h1>
                            ';
                        }

                        // If the order is payed a back button will be shown otherwise a back and pay button is shown
                        if(isset($_GET['betaald'])){
                            echo '
                                <div class="panel-footer">
                                    <a href="/" class="btn btn-dark my-5">Terug naar Chocoly</a>
                                </div>
                            ';
                        }
                        else {
                            echo '
                                <div class="panel-footer my-5">
                                    <a href="/bestellen" class="btn btn-secondary my-5">Terug</a>
                                    <a href="?betaald=true" class="btn btn-dark my-5 mx-2">Betalen</a>
                                </div>
                            ';
                        }
                    ?>
                </div>
                <div class="col-6">
                    <img src="<?=$link?>images/ideal-logo.png" alt="IDeal logo">
                </div>
            </div> 
        </div> 
    </div>
       
        
       
</body>
</html>
<?=isAuth();?>