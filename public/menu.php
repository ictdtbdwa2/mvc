<?php
    if(!isset($link)){
        $link = '';
    }
    if (isset($_POST['login'])) {
        login();
    }

    if (isset($_GET['signout'])) {
        signout();
    }

    if(isset($_SESSION['notification'])){
       setNotification($script, $link);
    }

?>
<div class="menu">
    <div class="container">
        <div class="row">
            <div class="menu-mobile menu-open" onclick="mobileMenu()">
                <div class="col-2">
                    <i class="fas fa-bars"></i>
                </div>
            </div>
            <a href="/" class="menu-logo"><img src="<?=$link?>images/chocoly-logo.svg" alt="Chocoly logo" class="logo"></a>
            <div class="menu-links" id="menu-links">
                <ul>
                    <li class="menu-link"><a href="/producten">Producten</a></li>
                    <li class="menu-link"><a href="/over-ons">Over ons</a></li>
                    <li class="menu-link"><a href="/contact">Contact</a></li>
                </ul>
                <ul class="menu-icons">
                    <form class="menu-search" id="menu-search" action="/producten" method="get">
                        <i class="fas fa-times menu-search-close" onclick="searchMenu()"></i><input type="text" name="search" value="<?=isset($_GET['search']) ? $_GET['search'] : ""?>" placeholder="Zoeken" required>
                    </form>
                    <li class="menu-link"><a href="javascript:void(0);" id="menu-seach-icon"><i class="fas fa-search"></i></a></li>
                    <li class="menu-link"><a href="/winkelwagen"><i class="fas fa-shopping-cart"></i></a></li>
                    <?php
                    // If user is not logged in the person icon in the menu shows on click a login popup
                    // Otherwise on click the browser redirects to the profile page and a logout button is shown in the menu
                    if(!isset($_SESSION['username'])){
                        echo '<li class="menu-link" onclick="loginPopup()"><a href="javascript:void(0)"><i class="fas fa-user"></i></a></li>';
                    }
                    else {
                        echo '<li class="menu-link"><a href="/profiel"><i class="fas fa-user"></i></a></li>
                        <li class="menu-link"><a href="?signout=true"><i class="fas fa-sign-out"></a></i></li>';
                    }
                    ?>
                </ul>
            </div>
            <form class="login-popup" id="login-popup" method="post">
                <div class="section-small">
                    <label>Email</label>
                    <input type="input" required value="<?=isset($_POST['user-email']) ? $_POST['user-email'] : ''?>" name="user-email">
                </div>
                <div class="section-small">
                    <label>Wachtwoord</label>
                    <input type="password" required name="user-password">
                    <?php
                        // Shows a error message on a wrong email or password combination
                        if(isset($_POST['authError'])){
                            echo '
                                <small class="text-danger d-block">Email of wachtwoord onjuist.</small>
                            ';
                            $_SESSION['script'] .= "<script>loginPopup()</script>";
                        }
                    ?>
                </div>
                <div class="section-small">
                    <input type="submit" name="login" value="login" class="btn btn-dark d-block">
                </div>
                <a href="/registratie" class="link">Account aanmaken</a>
            </form>
        </div>
    </div>
</div>
