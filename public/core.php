<?php
    session_start();
    $link = getLinkPrefix();
    define("LINK", $link);

    //makes a prexix for the links in the webshop
    function getLinkPrefix () {
        $linkprefix = '';
        //this function gets the url link after the hostname
        $urlArray = explode('/', substr($_SERVER['REQUEST_URI'],1));
        for($i = 1; $i < count($urlArray); $i++){
            $linkprefix .= '../';
        }
        return $linkprefix;
    }

    //include all functions in the functions directory
    foreach(glob($link."functions/*.php") as $function){
        require_once $function;
    }
?>