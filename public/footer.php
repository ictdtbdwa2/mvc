<?php
    if(!isset($footerMargin)){
        $footerMargin = '80px';
    }

    if (isset($_POST['subscripe'])){
        addNewsletter($_POST['email']);
    }
?>
<div class="footer" style="margin-top:<?=$footerMargin?>">
    <div class="container container-non">
        <div class="row">
            <div class="col-2">
                <a href="/"><img src="<?=$link?>images/chocoly-logo-white.svg" alt="Chocoly logo" class="footer-logo"></a>
            </div>
            <div class="col-2">
                <ul class="footer-links">
                    <li><a href="/producten">Producten</a></li>
                    <li><a href="/over-ons">Over ons</a></li>
                    <li><a href="/contact">Contact</a></li>
                </ul>
            </div>
            <div class="col-6">
                <div class="footer-newsletter">
                    <h4>Schrijf je in voor onze nieuwsbrief</h4>
                    <form action="" method="post">
                        <input required type="email" name='email' class="newsletter-input" placeholder="Email adres">
                        <input type="submit" name="subscripe" value="Schrijf me in" class="newsletter-submit">
                    </form>
                </div>
            </div>
            <div class="col-2">
                <div class="footer-contact">
                    <a href="tel:0505 66200">0505 66200</a>
                    <a href="mailto:support@chocoly.nl">support@chocoly.nl</a>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="<?=$link?>js/custom.js"></script>
<?=$_SESSION['script']?>