<?php
    include_once('../../core.php');
    $invoiceshtml = '';

    $id = $_GET['id'];
    $invoice = Select("select * from wideworldimporters.v_Choc_customerinvoices where customerid = ".$_SESSION['user_id']." and InvoiceID = ".$id)[0];
    $invoicelines = Select("select * from wideworldimporters.v_choc_customerinvoicedetails where invoiceid = ".$id);
    $tp = 0;
    
    // Retrieves all products that are in the order if no products are found a error message is shown
    if (count($invoicelines) > 0) {
        foreach ($invoicelines as $invoiceline) {
            $tp += number_format(number_format($invoiceline['unitpriceExclTaxSingle'] * 1.21, 2) * $invoiceline['quantity'],2);
            $invoiceshtml .= 
            '<div class="order">
                <div class="row">
                    <div class="col-2">
                        <img style="width:100%; height:100%; object-fit: contain;" src="'.$link.$invoiceline['filelocation'].'" alt="'.$invoiceline['filelocation'].'">
                    </div>
                    <div class="col-6">
                        <div class="section-tiny">
                            <label>Product</label>
                            <p>'.$invoiceline['description'].'</p>
                        </div>
                        <div class="section-tiny">
                            <label>Aantal</label>
                            <p>'.$invoiceline['quantity'].'</p>
                        </div>
                        <div class="section-tiny">
                            <label>Prijs per stuk</label>
                            <p>€ '.number_format($invoiceline['unitpriceExclTaxSingle'] * 1.21,2).'</p>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="h-100" style="display:flex; justify-content: center;  align-items: center;">
                            <a href="/producten/bekijk?id='.$invoiceline['stockitemid'].'" class="btn btn-dark px-4">Bekijk Product</a>
                        </div>
                    </div>
                </div>
            </div>';
        }
    }
    else {
        $invoiceshtml = 'Geen bestelling gevonden.';
    }
?>

<!DOCTYPE html>
<html lang="nl">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Chocoly</title>
    <link href="<?=$link?>images/favicon.svg" rel="icon" >
    <link href="<?=$link?>css/main.css" rel="stylesheet">
    <script src="<?=$link?>js/jquery.min.js"></script>
</head>
<body>
    <?php include($link."menu.php"); ?>
    <div class='container'>
        <div class="row">
            <div class="col-4">
                <div class="btn-menu">
                    <a href="../" class='btn btn-info text-light w-50 text-left'>
                        <h5 class="text-white">Terug</h5>
                    </a>
                </div>
            </div>
            <div class="col-7">
                <h2>Bestelling: #<?=$id?></h2>
                <hr>
                <div class="row">
                    <div class="col-6">
                        <b class='d-block section-tiny'>Besteldatum</b>
                        <?php
                            // Shows gift cost calculation if the order had a gift wrapping paper
                            // Other wise it just shows the total cost of the order
                            if ($invoice['isGift']){
                                echo "
                                    <b class='d-block section-tiny'>Bedrag</b>
                                    <b class='d-block section-tiny'>Cadeauverpakking</b>
                                    <b class='d-block'>Totaal bedrag</b>
                                ";
                            }
                            else {
                                echo "
                                    <b class='d-block'>Totaal bedrag</b>
                                ";
                            }
                        ?>
                    </div>
                    <div class="col-6">
                        <p class="section-tiny"><?=date_format(date_create($invoice['invoicedate']), "d-m-Y")?></p>
                        <?php
                            // Shows gift cost calculation if the order had a gift wrapping paper
                            // Other wise it just shows the total cost of the order
                            if ($invoice['isGift']){
                                echo "
                                    <p class='d-block section-tiny'>€ ".number_format($tp,2)."</p>
                                    <p class='d-block section-tiny'>€ ".number_format($invoice['isGift'], 2)."</p>
                                    <p class='d-block'>€ ".number_format(($tp + $invoice['isGift']), 2)."</p>
                                ";
                            }
                            else {
                                echo "
                                    <p class='d-block'>€ ".number_format($tp, 2)."</p>
                                ";
                            }
                        ?>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-6">
                        <b class='d-block section-tiny'>Straat</b>
                        <b class='d-block section-tiny'>Postcode</b>
                        <b class='d-block'>Plaats</b>
                    </div>
                    <div class="col-6">
                        <p class='d-block section-tiny'><?=$invoice['addressline1']?></p>
                        <p class='d-block section-tiny'><?=$invoice['postalcode']?></p>
                        <p class='d-block'><?=$invoice['city']?></p>
                    </div>
                </div>
                <hr>
                <?=  $invoiceshtml ?>
            </div>
        </div>
    </div>
    <?php include_once($link.'footer.php'); isAuth();?>
</body>
</html>