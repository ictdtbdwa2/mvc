<?php
    include_once('../core.php');
    $invoiceshtml = '';

    $invoices = Select("select * from wideworldimporters.v_Choc_customerinvoices where CustomerID = ".$_SESSION['user_id']. " ORDER BY invoiceid DESC");

    // Retrieves all orders that that exists if no orders are found a error message is shown
    if (count($invoices) > 0) {
        foreach ($invoices as $invoice) {
            $invoiceshtml .= 
            '<div class="order">
                <div class="row">
                    <div class="col-8">
                        <div class="section-tiny">
                            <label>Besteldatum</label>
                            <p>'.date_format(date_create($invoice['invoicedate']), "d-m-Y").'</p>
                        </div>
                        <div class="section-tiny">
                            <label>Bestellingnummer</label>
                            <p>'.$invoice['invoiceid'].'</p>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="h-100" style="display:flex; justify-content: center; align-items: center;">
                            <a href="bekijk?id='.$invoice['invoiceid'].'" class="btn btn-dark px-4">Bekijk bestelling</a>
                        </div>
                    </div>
                </div>
            </div>';
        }
    }
    else {
        $invoiceshtml = 'Geen bestellingen gevonden.';
    }
?>

<!DOCTYPE html>
<html lang="nl">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Chocoly</title>
    <link href="<?=$link?>images/favicon.svg" rel="icon" >
    <link href="<?=$link?>css/main.css" rel="stylesheet">
    <script src="<?=$link?>js/jquery.min.js"></script>
</head>
<body>
    <?php include($link."menu.php"); ?>
    <div class='container'>
        <div class="row">
            <div class="col-4">
                <div class="btn-menu">
                    <a href="/profiel" class='btn w-50 text-left'>
                        <h5>Profiel</h5>
                    </a>
                    <a href="/privacyinformatie" class='btn w-50 text-left'>
                        <h5>Privacy informatie</h5>
                    </a>
                    <a class='btn btn-info w-50 text-left'>
                        <h5 class="text-white">Bestellingen</h5>
                    </a>
                </div>
            </div>
            <div class="col-7">
                <h2 class="header">Bestellingen</h2>
                <?=  $invoiceshtml ?>
            </div>
        </div>
    </div>
    <?php include_once($link.'footer.php'); isAuth();?>
</body>
</html>