<?php
    include_once('core.php');

    if (isset($_POST['add'])){
        $products = addToCart($_POST['add']);
    }
    
    $products = Select('select * from wideworldimporters.v_choc_productpage limit 10');
    $html_result = '';
    // Places 10 products
    foreach($products as $product){
        $html_result .='<div class="col-xl-5th col-lg-4 col-md-6 col-xs-12">
            <div class="product">
                <div class="product-title">
                    <b>'.$product['stockitemname'].'</b>
                </div>
                <div class="product-image">
                    <img src="'.$product['defaultmedia'].'" alt="'.$product['stockitemname'].'">
                </div>
                <div class="product-bottom">
                    <h4 class="product-price"> € '.number_format($product['unitprice'] * 1.21, 2).'</h4>
                    <a href="/producten/bekijk?id='.$product['stockitemid'].'" class="btn btn-dark">Bekijk product</a>
                    <form class="d-inline-block" action="" method="post"> 
                        <input name="add" type="hidden" value='.$product['stockitemid'].'>
                        <button class="btn btn-icon"><i class="fas fa-shopping-cart"></i></button>
                    </form>
                </div>
            </div>
        </div>';
    }
?>

<!DOCTYPE html>
<html lang="nl">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Chocoly</title>
    <link href="images/favicon.svg" rel="icon" >
    <link href="css/main.css" rel="stylesheet">
    <script src="js/jquery.min.js"></script>
</head>
<body>
    <?php include("menu.php"); ?>
    <div class="banner"></div>
    <div class="products">
        <div class="container">
            <div class="row">
                <?php
                    echo $html_result;
                ?>
            </div>
        </div>
    </div>
    <div class="home">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <h2 class="header">Onze gekoelde chocola</h2>
                </div>
                <div class="col-lg-2 col-md-4 col-sm-12">
                    <img src="images/cooled-chocola.jpg" alt="Gekoelde chocola" class="full-img">
                </div>
                <div class="col-lg-10 col-md-8 col-sm-12">
                    <p class="text-container">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. 
    Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proiden.
    Sunt in culpa qui officia deserunt mollit anim id est laborum.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.
    Quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
                </div>
            </div>
        </div>
    </div>
<?php include_once('footer.php');?>
</body>
</html>