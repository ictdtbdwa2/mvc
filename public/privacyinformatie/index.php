<?php
    include_once('../core.php');
?>

<!DOCTYPE html>
<html lang="nl">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Chocoly</title>
    <link href="<?=$link?>images/favicon.svg" rel="icon" >
    <link href="<?=$link?>css/main.css" rel="stylesheet">
    <script src="<?=$link?>js/jquery.min.js"></script>
</head>
<body>
    <?php include($link."menu.php"); ?>
    <div class='container'>
        <div class="row">
            <div class="col-4">
                <div class="btn-menu">
                    <a href="/profiel" class='btn btn-dark w-50 text-left'>
                        <h5>Profiel</h5>
                    </a>
                    <a class='btn btn-info w-50 text-left'>
                        <h5 class="text-white">Privacy informatie</h5>
                    </a>
                    <a href="/bestellingen" class='btn btn-dark w-50 text-left'>
                        <h5>Bestellingen</h5>
                    </a>
                </div>
            </div>
            <div class="col-7">
                <h2 class="header">Privacy informatie</h2>
                <div class='text-container'>
                    <p class='text-white'>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis viverra convallis mattis. Sed et ipsum in lectus vehicula pretium dictum congue tellus. Curabitur molestie ullamcorper ante suscipit tristique. Sed lacus nunc, imperdiet non dui nec, euismod dapibus dolor. Ut et mi justo. Cras consectetur sodales sem. Aliquam et accumsan velit, sed ornare massa. Nunc semper, eros id rhoncus aliquam, tortor augue egestas felis, et posuere augue quam in elit. Mauris ex magna, venenatis sit amet nisi tempor, tristique iaculis diam. Proin lorem ante, dignissim sed ligula sit amet, scelerisque dictum metus.
                    <br><br>
                    Donec a est sodales mauris lobortis cursus. Curabitur enim eros, dignissim id nulla at, elementum laoreet augue. Morbi tortor nisi, facilisis eu mi at, iaculis ullamcorper sapien. Sed quis dignissim nibh. Donec ullamcorper faucibus laoreet. Cras quis dignissim orci. Maecenas sit amet porttitor libero. Vivamus molestie justo libero, in porttitor quam tempus ut. Sed euismod augue risus, sed interdum dolor tempor non.
                    <br><br>
                    Nam porttitor tellus erat, vitae imperdiet risus fringilla nec. Duis vulputate purus pellentesque tellus ornare, vel suscipit quam iaculis. Nunc vel tempor augue. Integer nisi risus, commodo ac elit id, egestas bibendum est. Ut arcu dui, condimentum vitae varius ut, iaculis ac eros. Donec odio metus, porta ac auctor vitae, semper sed neque. Donec interdum elementum dictum. Aenean vulputate lorem a elit ultricies, vel vestibulum justo suscipit. Sed ante sem, ornare aliquam massa eget, ultricies iaculis lorem. Aliquam justo urna, ultrices non tortor vitae, ultrices interdum mauris. Cras pulvinar quam sed porta iaculis. Etiam id blandit diam.
                    <br><br>
                    Praesent suscipit dolor vitae metus pharetra consectetur. Aliquam erat volutpat. Sed et libero et magna malesuada porttitor. Interdum et malesuada fames ac ante ipsum primis in faucibus. Suspendisse imperdiet augue nec egestas pretium. Nulla porta dui sed nisl auctor, ac sollicitudin lorem iaculis. Curabitur volutpat ut mi sit amet eleifend. Curabitur ullamcorper purus augue, accumsan pellentesque sapien porta et. Cras nec lacus ac mauris condimentum luctus in quis urna. Sed vulputate nunc vitae tellus scelerisque molestie.
                    <br><br>
                    Curabitur id scelerisque arcu. Pellentesque at semper dui, sed interdum lorem. Pellentesque commodo aliquet purus, lobortis dapibus elit. Nulla facilisi. Ut nunc nulla, euismod sed ultricies non, tempor eu libero. Nulla euismod vel erat ac consequat. Nunc posuere velit eu risus consequat, quis iaculis nisi lacinia. Suspendisse massa mauris, viverra sit amet arcu vel, mollis feugiat ante. Integer accumsan mauris enim, dignissim sodales libero ultricies vel. Nullam malesuada pellentesque sem id porttitor. Praesent sapien quam, consectetur at odio id, elementum hendrerit massa. Nam interdum, tellus sit amet aliquet euismod, risus purus accumsan tortor, sit amet tincidunt nunc lacus nec velit.
                    </p>
                </div>
            </div>
        </div>
    </div>
    <?php include_once($link.'footer.php'); isAuth();?>
</body>
</html>