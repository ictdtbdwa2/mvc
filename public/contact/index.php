<?php 
    include_once('../core.php');

    $footerMargin = '0px';

    if (isset($_POST['message'])){
        setNotification("Bericht verzonden.");
    }

?>
<!DOCTYPE html>
<html lang="nl">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Chocoly</title>
    <link href="<?=$link?>images/favicon.svg" rel="icon" >
    <link href="<?=$link?>css/main.css" rel="stylesheet">
    <script src="<?=$link?>js/jquery.min.js"></script>
</head>
<body>
<?php include($link."menu.php"); ?>
    <div class="contact">
        <div class="container">
            <div class="row">
                <div class="col-5">
                    <!------------------------------------------------------
                    -------------------------------------------------------- 
                    -- This form is used to show the messagebox. 
                    -- The user must fill in the following field:
                    -- name, email and messagebox. These fields cannot 
                    -- not be null
                    --------------------------------------------------------
                    -------------------------------------------------------->
                    <form action="" method="post">
                        <h2 class="header">Contact</h2>
                        <div class="contactinputbox-l w-100">
                            <label>Naam</label>
                            <input required class="contacttext" type="text">
                        </div>
                        <div class="contactinputbox-l w-100">
                            <label>Email</label>
                            <input required class="contacttext" type="email">
                        </div>
                        <div class="contactinputbox-xl w-100">
                            <label>Bericht</label>
                            <textarea required class="contactmessage" min='50' cols="30" rows="10"></textarea>
                        </div>
                        <div class="d-block">
                            <input type="submit" name="message" class="btn btn-dark" value="Bericht verzenden">
                        </div>
                    </form>
                </div>

                <!------------------------------------------------------
                -------------------------------------------------------- 
                -- This box show the static contactinfo with the fiels: 
                -- phone number, emailaddress and address.
                --------------------------------------------------------
                -------------------------------------------------------->
                <div class="col-3 offset-md-4">
                    <div class="contact-information">
                        <div class="section-small">
                            <label>Telefoonnummer:</label>
                            <a href="tel:0505 66200">0505 66200</a>
                        </div>
                        <div class="section-small">
                            <label>Email adres:</label>
                            <a href="mailto:support@chocoly.nl">support@chocoly.nl</a>
                        </div>
                        <div>
                            <label>Adres:</label>
                            <p>Bloemstraat 122<br>
                               8080 AB ZWOLLE 
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div style="margin-top: 50px">
        <iframe style="width: 100%; height: 250px; margin: 0; padding: 0; line-height: 0" id="gmap_canvas" src="https://maps.google.com/maps?q=Amsterdam%2&amp;t=&amp;z=15&amp;ie=UTF8&amp;iwloc=&amp;output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
    </div>
</div>
    <?php include_once($link.'footer.php');?>
</body>
</html>