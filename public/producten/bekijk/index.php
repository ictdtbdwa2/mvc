<?php
    include_once('../../core.php');

    if (isset($_POST['add'])){
        $products = addToCart($_POST['add']);
    }
    
    $product = Select("select * from wideworldimporters.v_choc_productpage where stockitemid = ".injectionProtection($_GET['id'])." limit 1")[0];
    $images = Select("select * from wideworldimporters.v_choc_productmedia where stockitemid = ".injectionProtection($_GET['id'])." ");
    $imageshtml = '';

     //All the images will be shown
    foreach($images as $image){
        $imageshtml .= '
        <div class="col-4">
            <div class="product-image-small">
                <img class="productImages" src="'.$image['filelocation'].'" alt="'.$image['Description'].'">
            </div>
        </div>
        ';
    }
    
?>
<!DOCTYPE html>
<html lang="nl">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Chocoly</title>
    <link href="<?=$link?>images/favicon.svg" rel="icon" >
    <link href="<?=$link?>css/main.css" rel="stylesheet">
    <script src="<?=$link?>js/jquery.min.js"></script>
</head>
<body>
    <?php include($link."menu.php"); ?>
    <div class="container">
        <div class="row">
            <div class="col-7">
                <div class="row">
                    <div class="col-12">
                        <div class="product-image-big">
                            <img id="bigImage" src="<?=$images[0]['filelocation']?>" alt="<?=$images[0]['Description']?>">
                        </div>
                       
                    </div>
                </div>
                <div class="row">
                    <?php
                        echo $imageshtml;
                    ?>
                    
                </div>
            </div>
            <div class="col-5">
                <div class="product-information">
                    <div class="title section">
                        <h5 class="text-center text-white"><?= $product['stockitemname'] ?></h5>
                    </div>
                    <div class="section">
                        <p><?= $product['description'] ?></p>
                    </div>
                    <div class="section">
                        <h2 class="text-center">€ <?= number_format($product['unitprice'] * 1.21, 2) ?></h2>
                    </div>
                    <div class="section">
                        <label>Smaken</label>
                        <p><?= $product['Tastes'] ?></p>
                    </div>
                    <div class="section">
                        <label>Categorieën</label>
                        <p><?= $product['Categories'] ?></p>
                    </div>
                    <div class="section">
                        <form action="" method="post">
                            <input type="hidden" name="add" value="<?= $product['stockitemid'] ?>">
                            <input class="text-center" name="amount" type="number" min=1 value=1> 
                            <button class="btn btn-dark">Toevoegen aan winkelwagen</button>
                        </form>
                    </div>
                    <?php 
                        // if a product is placed in a shopping cart: a message will be shown.
                        if(isset($products[$_GET['id']])){
                            echo '
                                <div class="text-success">
                                    Je hebt al '.$products[$_GET['id']]['amount'].' stuks van dit product in je winkelwagen zitten.
                                </div>
                            ';
                        }
                    ?>
                </div>
            </div>
        </div>
    </div>
    <?php include_once($link.'footer.php');?>
</body>
</html>