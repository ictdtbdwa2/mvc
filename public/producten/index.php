<?php 
    include_once('../core.php');

    if (isset($_POST['add'])){
        $products = addToCart($_POST['add']);
    }

    $gcategorie = isset($_GET['category']) ? injectionProtection($_GET['category']) : "";
    $gsearch = isset($_GET['search']) ? injectionProtection($_GET['search']) : "";
    $gtaste = isset($_GET['taste']) ? injectionProtection($_GET['taste']) : "";
    // variable for the HTML code will be set.
    $categorie_result = '';
    // variable for the HTML code will be set.
    $taste_result = '';
    $products = null;

     /* ==============================
    ======= Author: Mark de Bruin
    ======= Description: Retrieving all the categorynames of the Chocoly products. This will be placed in a dropdown selectionbox
    ========*/

    // retrieves all the available categories
    $categorySelection = Select("SELECT t1.StockGroupName FROM wideworldimporters.Stockgroups t1 WHERE t1.UsageType = 'Chocoly' AND EXISTS (SELECT 's' FROM wideworldimporters.stockItemStockgroups t2 WHERE t1.Stockgroupid=t2.Stockgroupid)");
    
    foreach($categorySelection as $category){
        // If the category is already selected
        if ($category['StockGroupName'] === $gcategorie) {
            $categorie_result .= '<option selected value="'.$category['StockGroupName'].'">'.$category['StockGroupName'].'</option>';
        }
        // If not (or nothing selected)
        else {
            $categorie_result .= '<option value="'.$category['StockGroupName'].'">'.$category['StockGroupName'].'</option>';
        }
    }

    /* ==============================
    ======= Author: Mark de Bruin
    ======= Description: Retrieving all the tastes of the Chocoly products. This will be placed in a dropdown selectionbox
    ========*/

    // retrieves all the available tastes
    $tasteSelection = Select("SELECT t1.StockTasteName FROM wideworldimporters.stocktaste t1 WHERE t1.UsageType = 'Chocoly' AND EXISTS (SELECT 's' FROM wideworldimporters.stockItemstocktaste t2 WHERE t1.stocktasteid=t2.stocktasteid)");


    foreach($tasteSelection as $taste){
        // If the taste is already selected
        if ($taste['StockTasteName'] === $gtaste) {
            $taste_result .= '<option selected value="'.$taste['StockTasteName'].'">'.$taste['StockTasteName'].'</option>';
        }
        // If not (or nothing selected)
        else {
            $taste_result .= '<option value="'.$taste['StockTasteName'].'">'.$taste['StockTasteName'].'</option>';
        }   
    }

     /* ==============================
    ======= Author: Mark de Bruin
    ======= Description: Retrieving the productinformation of the selected products
    ========*/
    // If the category is already set OR there is a searchterm OR If the taste is already set
    if(isset($_GET['category']) || isset($_GET['search']) || isset($_GET['taste'])){
        // explodes the $gsearch (makes an array)
        $gsearch = explode (" ", $gsearch);
        // variable for the SQL criteria code will be set.
        $searchsql = '';
        
        foreach($gsearch as $i => $search){
            //The like criteria will be set
            $searchsql .= "AND stockitemname LIKE '%".$search."%' ";
        }
        // the select statement will be executed.
        $products = Select("SELECT * FROM wideworldimporters.V_Choc_ProductPage WHERE categories LIKE '%".$gcategorie."%' ".$searchsql." AND tastes LIKE '%".$gtaste."%'");
    } else {
        // the select statement will be executed.
        $products = Select('SELECT * FROM wideworldimporters.V_Choc_ProductPage');
    }

    /* ==============================
    ======= Author: Mark de Bruin
    ======= Description: Shows the selected products
    ========*/
    $html_result = '';
     // All (selected) products will be shown.
     if(count($products) > 0){ 
        foreach($products as $product){
            $html_result .= '<div id="'.$product['stockitemid'].'" class="row product">
            <div class="col-2">
                <img src="'.$product['defaultmedia'].'" alt="'.$product['stockitemname'].'">
            </div>
            <div class="col-7">
                <h5 class="header">
                    <b>'.$product['stockitemname'].'</b>
                </h5>
                <p>'.$product['description'].'</p>
            </div>
            <div class="col-3">
                <h2 class="cost">€ '.number_format($product['unitprice'] * 1.21 ,2).'</h2>
                <form action="#'.$product['stockitemid'].'" method="post"> 
                    <input name="add" type="hidden" value='.$product['stockitemid'].'>
                    <a href="bekijk?id='.$product['stockitemid'].'" class="btn btn-dark">Bekijk product</a><button class="btn-icon"><i class="fas fa-shopping-cart"></i></button>
                </form>
            </div>
        </div>';
        }
    } else {
        $html_result = '<h4 class="text-center">Geen producten gevonden</h4>';
    }

?>
<!DOCTYPE html>
<html lang="nl">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Chocoly</title>
    <link href="<?=$link?>images/favicon.svg" rel="icon" >
    <link href="<?=$link?>css/main.css" rel="stylesheet">
    <script src="<?=$link?>js/jquery.min.js"></script>
</head>
<body>
    <?php include($link."menu.php"); ?>
    <div class="container">
        <div class="row">
            <div class="col-2">
                <div class="filterbox">
                    <form action="" method="GET">
                        <div class="section">
                            <label>Zoeken:</label>
                            <input class="w-100" type="text" name="search" value="<?php echo isset($_GET['search']) ? $_GET['search'] : ""?>">
                        </div>
                        <div class="section">
                            <label>Categorie:</label>
                            <select class="w-100" name="category" id="category">
                                <option value="">Kies categorie</option>
                                <?php 
                                    echo $categorie_result;
                                ?>
                            </select>
                        </div>
                        <div class="section">
                            <label>Smaak:</label>
                            <select class="w-100" name="taste" id="taste">
                                <option value="">Kies smaak</option>
                                <?php 
                                    echo $taste_result;
                                ?>
                            </select>
                        </div>
                        <div class="text-right">
                            <input class="btn btn-dark w-100" name='submit' value="Filter" type="submit">
                        </div>
                        
                    </div>
                </form>
            </div>  
            <div class="col-10">
                <div class="searchresults product-container">
                    <div class="container container-non">
                    <?php
                        echo $html_result;
                    ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php include_once($link.'footer.php');?>
</body>
</html>
