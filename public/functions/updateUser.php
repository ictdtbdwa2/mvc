<?php
    // Updates the user information
    function updateUser ($userinformation, $link = "../../"){
        $id = $_SESSION['user_id'];
        $result = Select("SELECT * FROM wideworldimporters.v_Choc_profilepage WHERE customerid != ".$_SESSION["user_id"]." AND emailaddress = '".injectionProtection($userinformation['emailaddress'])."'");

        // Checks if the new emailaddress already exits if so shows a message
        if ($result){
            setNotification("Email bestaat al.");
            return;
        }

        $profile = Select("SELECT * FROM wideworldimporters.v_Choc_profilepage WHERE customerid = ".$_SESSION["user_id"]." limit 1")[0];
        $sql_newsletter = "DELETE FROM wideworldimporters.newsletter WHERE (EmailAddress = '".injectionProtection($profile['emailaddress'])."')";

        // Deletes the user emailaddress from the newsletter table in the database
        // If no connection can be made a message is shown
        if($res = updateDeleteInsert($sql_newsletter)){ 
            setNotification("Er is een fout ontstaan. <br>".$res, "true");
            return;
        }

        // Adds the user email to the newsletter table
        if (isset($userinformation['newsletter'])){
            addNewsletter($userinformation['emailaddress']);
        }

        $sql_customer = "UPDATE wideworldimporters.customers SET 
                        CustomerName = '".injectionProtection($userinformation['customerName'])."', 
                        EmailAddress = '".injectionProtection($userinformation['emailaddress'])."'
                        WHERE CustomerID = ".$id;

        // Updates the user name and email in the customer table
        if($res = updateDeleteInsert($sql_customer)){ 
            setNotification("Er is een fout ontstaan. <br>".$res, "true");
            return;
        }

        setNotification("Profiel is geupdate");

    }
?>