<?php
    // Checks if the user is not logged in if so removes all get data from url
    function isAuth () {
        if (!isset($_SESSION['user_id'])){
            echo '
                <script>
                    location.replace("/");
                </script>
            ';
        }
    }

    function isNotAuth(){
        if (isset($_SESSION['user_id'])){
            echo '
                <script>
                    location.replace("/");
                </script>
            ';
        }
    }
    

    // Authenticates the user submitted email and password
    function login() {
        $_POST['authError'] = null;
        
        // Checks if the email and password are both submitted
        if (isset($_POST["user-email"]) && isset($_POST["user-password"])) {
            $email = injectionProtection($_POST["user-email"]);
            $password = injectionProtection($_POST["user-password"]);
            
            // Connects to the database and searches for the email in the customer table
            $users = Select("select CustomerID, CustomerName, EmailAddress, HashedPassword from wideworldimporters.customers where lower(EmailAddress) = lower('$email')");
            $user = $users != null ? $users[0] : null;

            if(isset($user)){
                $pass = checkpassword($user['HashedPassword'], $password);
                if ($pass) {
                    $_SESSION['username'] = $user['CustomerName'];
                    $_SESSION['user_id'] = $user['CustomerID'];
                    setNotification("Je bent ingelogd.");
                }
                else {
                    $_POST['authError'] = true;
                    setNotification("Inloggen is niet gelukt.");
                }
            }
            else {
                $_POST['authError'] = true;
                setNotification("Inloggen is niet gelukt.");
            }
           

            // Checks if the email and password matches if they do sets a session user name and id and notifies the user that the login was a succes
            // Otherwise it shows a error message
           
        }
    }

    // Verifies the inputted password with the password from the database
    function checkpassword ($hashedPassword, $password) {
        return password_verify($password, $hashedPassword);
    }

    // Removes the session data loggin and shows a message to the user
    function signout () {
        unset($_SESSION['username']);
        unset($_SESSION['user_id']);
        $protocal   = $_SERVER['REQUEST_SCHEME'];
        $host       = $_SERVER['HTTP_HOST'];
        echo '
            <script> 
                let res = window.location.href.replace("?signout=true", "");
                res = res.replace("'.$host.'", ""); 
                res = res.replace("'.$protocal.'", ""); 
                res = res.replace(":///", ""); 
                window.history.replaceState({}, document.title, "/" + res);
            </script>
        ';
        setNotification("Je bent uitgelogd.");
    }
?>


