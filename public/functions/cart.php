<?php
    if(!isset($_SESSION["shopping_cart"])){
        $_SESSION["shopping_cart"] = [];
    }

    $products = $_SESSION["shopping_cart"];

    /*
        Remove a product from shopping cart
    */
    function removeFromCart($id) {
        if (isset($_SESSION["shopping_cart"][$id])){
            unset($_SESSION["shopping_cart"][$id]);
            setNotification("Product verwijderd uit winkelwagen.");
        }

        return $_SESSION["shopping_cart"];
    }

    /*
        Change the amount of the product
    */
    function changeFromCart ($id) {
        if (isset($_SESSION["shopping_cart"][$id])){
            $_SESSION["shopping_cart"][$id]['amount'] = $_POST['amount'];
        }

        return $_SESSION["shopping_cart"];
    }

    /*
        Add a product to the shopping cart
    */
    function addToCart ($id) {
        $amount = isset($_POST['amount']) ? $_POST['amount']: 1;
        if(isset($_SESSION["shopping_cart"][$id])){
            $_SESSION["shopping_cart"][$id]['amount'] += $amount;
        }
        else {
            $product = Select("select * from wideworldimporters.v_choc_productpage where stockitemid = ".$_POST['add']." limit 1")[0];
            $product['amount'] = $amount;
            $_SESSION['shopping_cart'][$id] = $product;
        }
        setNotification("Product toegevoegd aan winkelwagen.");
        

        return $_SESSION["shopping_cart"];
    }
    
    
    $products = $_SESSION["shopping_cart"];

    
?>