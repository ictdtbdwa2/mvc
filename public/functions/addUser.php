<?php
    include_once($link.'functions/addAddress.php');
    /* ===============================================================
    ======== Author:        Mark de Bruin
    ======== Decription:    This will create a new user of the webshop (table customers). Also the address will be inserted in the table .. and the 
    ========                addresslink table will also be filled.
    ======== How:           1.      Its uses 1 parameter. The POST/GET array
    ========                2.      It will check if the emailaddress is already in use.
    ========                            If so, It will return an errormessage that will be displayed at the bottom of the page (end of function). 
    ========                            If not it will continue with step 3
    ========                3.      The new CustomerID will be generated 
    ========                4.      The SQL insert statement will be generated (Customer table)
    ========                5.      The SQL insert statement will be executed and it will be immediately 
    ========                        checked if it went correct. A message will be generated if something went wrong.
    ========                6.      function addAddress will be called (creates a new deliveryaddress(if necassary) and a new link in the 
    ========                        customerdeliveryaddresses table)
    ========                7.      A notification will be shown that the account has been created. 
    ========*/
    function addUser($userInformation)
    {
        // This will result in a set variable if the emailaddress is already known 
        $UseralreadyExists = false; 
       
        // retrieves the emailaddres (if already exists)
        if(isset($userInformation['email'])) {
            $UseralreadyExists = select("select * from customers where lower(emailaddress) = lower('".injectionProtection($userInformation['email'])."')");
        }
        
        // Checks if the user is already known
        if(!empty($UseralreadyExists)) {   
            setNotification("Emailadres is al in gebruik.");
            return false;
        }
        else {
            // The new customerID will be retrieved
            $maxCustomerID = select("select max(customerid) MaxUserID from customers");
            $NewCustomerID = (($maxCustomerID[0]['MaxUserID'])+1);
                
            // Used as the insert statement for the Customers table 
            $sql_userinsert =       "INSERT INTO  Customers(CustomerID, CustomerName, BillToCustomerID, CustomerCategoryID, BuyingGroupID, PrimaryContactPersonID, 
                                    AlternateContactPersonID, DeliveryMethodID, DeliveryCityID, PostalCityID, CreditLimit, AccountOpenedDate, StandardDiscountPercentage,
                                    IsStatementSent, IsOnCreditHold, PaymentDays, PhoneNumber, FaxNumber, DeliveryRun, RunPosition, WebsiteURL, DeliveryAddressLine1,
                                    DeliveryAddressLine2, DeliveryPostalCode, DeliveryLocation, PostalAddressLine1, PostalAddressLine2, PostalPostalCode, LastEditedBy, 
                                    ValidFrom, ValidTo, UsageType, EmailAddress, HashedPassword
                                    )
                                    VALUES
                                    (".  
                                    /*CustomerID*/ $NewCustomerID.", ".
                                    /*CustomerName*/ "'".$userInformation['firstname']." ".$userInformation['insertion']." ".$userInformation['lastname']."', ".
                                    /*BillToCustomerID*/ $NewCustomerID.", ".
                                    /*CustomerCategoryID*/ "9,".
                                    /*BuyingGroupID*/ "3, ".
                                    /*PrimaryContactPersonID*/ "3262, ".
                                    /*AlternateContactPersonID*/ "3262, ".
                                    /*DeliveryMethodID*/ "5, ".
                                    /*DeliveryCityID*/ "38187, ".
                                    /*PostalCityID*/ "38187, ".
                                    /*CreditLimit*/ "null, ".
                                    /*AccountOpenedDate*/ "now(), ".
                                    /*StandardDiscountPercentage*/ "0, ".
                                    /*IsStatementSent*/ "0, ".
                                    /*IsOnCreditHold*/ "0, ".
                                    /*StandardDiscountPercentage*/ "0, ".
                                    /*PhoneNumber*/ "'Not relevant', ".
                                    /*FaxNumber*/ "'Not relevant', ".
                                    /*DeliveryRun*/ "null, ".
                                    /*RunPosition*/ "null, ".
                                    /*WebsiteURL*/ "'Not relevant', ".
                                    /*DeliveryAddressLine1*/ "'".ucfirst(strtolower($userInformation['street']))." ".$userInformation['housenumber']."', ".
                                    /*DeliveryAddressLine2*/ "'null', ".
                                    /*DeliveryPostalCode*/ "'".$userInformation['postalcode']."', ".
                                    /*DeliveryLocation*/ "'".ucfirst(strtolower($userInformation['city']))."', ".
                                    /*PostalAddressLine1*/ "'".ucfirst(strtolower($userInformation['street']))." ".$userInformation['housenumber']."', ".
                                    /*PostalAddressLine2*/ "'null', ".
                                    /*PostalPostalCode*/ "'".$userInformation['postalcode']."', ".
                                    /*LastEditedBy*/ "3262, ".
                                    /*ValidFrom*/ "now(), ".
                                    /*ValidTo*/ "'9999-12-31 23:59:59.0', ".
                                    /*UsageType*/ "'Chocoly', ".
                                    /*EmailAddress*/ "'".$userInformation['email']."', ".
                                    /*HashedPassword*/"'".password_hash($userInformation['password1'], PASSWORD_DEFAULT)."')";
            
            // Inserting the new customer row and immediately checks if it went succesfull                
            if($res = updateDeleteInsert($sql_userinsert)){   
                setNotification("Toevoegen van dit account is mislukt.<br> ".$res, "true");
                return false;
            }

            //add address to user
            addAddress($NewCustomerID, $userInformation);

            // Based on the errorflag a message will be generated
            setNotification("Account is aangemaakt.");
            return true;
        }
    }
?>