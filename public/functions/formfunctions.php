<?php

    /* ===============================================================
    ======== Author:        Mark de Bruin
    ========
    ======== Decription:    This function generates a form based on the input parameter $formtype. At this moment 
    ========                the only supported form is the 'registration' form. at the end it will return the form.
    ========
    ======== How:           1.  $correctform variable is created to see if the filled form has acceptable values.
    ========                2.  A 2D array $formheaders will be generated with all the values for each form element.
    ========                3.  It will loop through the 2D array $formheaders
    ========                    3.1     It will check if the form has been submitted.
    ========                        3.1.1. Each key in the 2D arary $formheaders will be checked
    ========                        3.1.2. Passwords will be checked on the following criteria:
    ========                                -   Not empty
    ========                                -   Must match the second one
    ========                                -   is minimal 10 digits long including one digit and one special character
    ========                                The errormessage will be written in: $formheaders[$formheader][6]
    ========                        3.1.3.  The other values will be matched with the function: generateFormErrormessage.
    ========                                This function will return an error message.
    ========                                The errormessage will be written in: $formheaders[$formheader][6]
    ========                        3.1.3.  If $formheaders[$formheader][6] is not null then the errorprefix for the css
    ========                                class will be filled in: $formheaders[$formheader][5]
    ========                        3.1.4.  The value $correctform will set to false.  
    ========                4.  It will check if $correctform is true and that the variable $currentform is set. If not step 5
    ========                    4.1.    It will check if $formtype is filled with 'registration') If not, 4.2.1.
    ========                        4.1.1.  The function addUser will be used the to create the user.
    ========                    4.2.    It will check if $_SESSION['notification'] <> "Account is aangemaakt.". This value
    ========                            will be set if the creation of the user went well.
    ========                        4.2.1.  It will generate the errormessage generated in the function Adduser.
    ========                5.	It will loop through the 2D array $formheaders
    ========                    5.1.	Each formtype will be handled
    ========                    5.2.	It will check if the input-field is 'message' if not step 5.3.
    ========                        5.2.1.	If the input-field 'message' is empty the inputbox will be 
    ========                            filled with placeholdertext.
    ========                        5.2.2.	If the input-field 'message' is not empty the messagebox 
    ========                            will be filled with the existing value.
    ========                        5.2.3.	Continue step 5.4
    ========                    5.3.	The inputboxes including the header will be created. The inputboxes will 
    ========                        be created in one of the following ways:
    ========                        5.3.1.	If no value has been set it will show with a placeholdertext
    ========                        5.3.2.	If the form is incorrectly filled in than it will be filled 
    ========                            in with the existing value
    ========                        5.3.3.	if the form is correctly filled the inputbox will be shown without 
    ========                            any value.
    ========                    5.4	An error message will be shown if the inputfield has an error within it.
    ========                6.	The comfirm button will be generated
    ========                7.	If the creation of the account as gone wrong, the error message will be generated.                 
    ========*/
    function generateForm($formtype, $currentform)
    {

        // This array is used to build forms. The order of entries is the order of the form inputboxes
        $correctform =  true;
        $formheaders =  array(
                            'RegFirstname' => array('registration', 'Voornaam', 'registrationinputbox-m', 'firstname', 'text', null, null, 'registrationtext'),
                            'RegInsertion' => array('registration', 'Tussenvoegsel', 'registrationinputbox-s', 'insertion', 'text', null, null, 'registrationtext'),
                            'RegLastname' => array('registration', 'Achternaam', 'registrationinputbox-m', 'lastname', 'text', null, null, 'registrationtext'),
                            'RegStreet' => array('registration', 'Straatnaam', 'registrationinputbox-l', 'street', 'text', null, null, 'registrationtext'),
                            'RegHousenumber' => array('registration', 'Nr.', 'registrationinputbox-s', 'housenumber', 'text', null, null, 'registrationtext'),
                            'RegPostalcode' => array('registration', 'Postcode', 'registrationinputbox-m', 'postalcode', 'text', null, null, 'registrationtext'),
                            'RegCity' => array('registration', 'Woonplaats', 'registrationinputbox-l', 'city', 'text', null, null, 'registrationtext'),
                            'RegEmail' => array('registration', 'Emailadres', 'registrationinputbox-m', 'email', 'email', null, null, 'registrationtext'),
                            'RegPassword1' => array('registration', 'Wachtwoord', 'registrationinputbox-m', 'password1', 'password', null, null, 'registrationtext'),
                            'RegPassword2' => array('registration', 'Herhaal wachtwoord', 'registrationinputbox-m', 'password2', 'password', null, null, 'registrationtext'),
        );

        $html = '<form action="" method="POST" autocomplete="off">';
        $html .= '<div class="registrationblock">';
        
        //It will loop through the 2D array $formheaders
        foreach($formheaders as $formheader => $formvalues){
            // It will check if the form has been submitted.
            if (isset($currentform['sendregistration']) && !isset($_POST['login'])){
 
                // Each key in the 2D arary $formheaders will be checked (which is used in the form)
                if($currentform && $formtype == $formvalues[0]){
                    // Passwords will be checked on the following criteria:
                    if($formheader == 'RegPassword1' || $formheader == 'RegPassword2'){
                        // Not empty
                        if (empty($currentform['password1'])|| empty($currentform['password2'])){
                            // The errormessage will be written in: $formheaders[$formheader][6]
                            $formheaders[$formheader][6] = 'Wachtwoord veld is leeg';
                        }
                        // Must match the second one
                        elseif($currentform['password1'] <> $currentform['password2']){
                            // The errormessage will be written in: $formheaders[$formheader][6]
                            $formheaders[$formheader][6] = 'Wachtwoord komt niet overeen';
                        }
                        // is minimal 10 digits long including one digit and one special character
                        elseif(!preg_match('/^(?=.*\d)(?=.*[A-Za-z])[0-9A-Za-z!@#$%&*]{10,99}$/', $currentform['password1'])) {
                            // The errormessage will be written in: $formheaders[$formheader][6]
                            $formheaders[$formheader][6] = 'Minimaal 10 karakters, 1 speciaal teken en 1 cijfer';
                        }
                    }
                    // The other values will be matched with the function: generateFormErrormessage.
                    elseif(isset($currentform[$formvalues[3]])){
                            //The errormessage will be written in: $formheaders[$formheader][6]
                            $formheaders[$formheader][6] = generateFormErrormessage($formvalues[3],$currentform[$formvalues[3]]);
                    }
                }
                //If $formheaders[$formheader][6] is not null then the errorprefix for the css class will be filled in: $formheaders[$formheader][5]
                if(isset($formheaders[$formheader][6])){
                    //The value $correctform will set to false.  
                    $correctform =  false;
                    $formheaders[$formheader][5] = 'i-';
                }
            }
            else {
                //The value $correctform will set to false.  
                $correctform =  false;
            }
        }
        // It will check if $correctform is true and that the variable $currentform is set. 
        if ($correctform && isset($currentform)){
            //It will check if $formtype is filled with 'registration')
            if($formtype == 'registration'){
                //The function addUser will be used the to create the user.
                $correctform = addUser($currentform);
            }
        }
        
        // It will loop through the 2D array $formheaders
        foreach($formheaders as $formheader => $formvalues){
            // Each formtype will be handled
            if($formtype == $formvalues[0]){
                //It will check if the input-field is 'message'
                if($formvalues[4] == 'message'){
                    $html .=     "<div class=".$formvalues[2].">
                            <h4>".$formvalues[1]."</h4>";
                    
                    //If the input-field 'message' is empty the inputbox will be filled with placeholdertext.
                    if(empty($currentform['message'])){
                        $html .=     "<textarea class=".$formvalues[5].$formvalues[7]." type=".$formvalues[4]." name=".$formvalues[3].">";
                    }
                    // If the input-field 'message' is not empty the messagebox will be filled with the existing value.
                    else {
                        $html .=     "<textarea class=".$formvalues[5].$formvalues[7]." type=".$formvalues[4]." name=".$formvalues[3].">".$currentform['message']."</textarea>";
                    }
                    $html .=  "</textarea>";
                }
                else {
                    // The inputboxes including the header will be created. The inputboxes will be created in one of the following ways:
                    $html .=  '<div class="'.$formvalues[2].'"><label>'.$formvalues[1].'</label><input class="'.$formvalues[5].$formvalues[7].'" name="'.$formvalues[3].'" type="'.$formvalues[4].'"';

                    //If no value has been set it will show with a placeholdertext
                    if(empty($currentform[$formvalues[3]])) {
                        $html .=  'placeholder="'.ucfirst(strtolower($formvalues[1])).'">';
                    }
                    // If the form is incorrectly filled in than it will be filled in with the existing value
                    elseif(!$correctform) {
                        $html .=   'value="'.$currentform[$formvalues[3]].'">';
                    }
                    // if the form is correctly filled the inputbox will be shown without any value.
                    else {
                        $html .=   'value="">';
                    }
                }
                if(isset($formvalues[6])) {
                    $html .=  '<p class="errormessage">'.$formvalues[6].'</p>';
                }
                // An error message will be shown if the inputfield has an error within it.
                $html .=  '</div>';
            }
    
        }

        
        $html .=  '</div>';
        //The comfirm button will be generated. For the form 'registration' it will have the value 'Registreer nu'. If not 'Verstur bericht'
        if($formtype == 'registration'){
                $html .=  '<div class="registrationblock"><input class="btn btn-dark" type="submit" name="sendregistration" value="Registreer nu"><div>';
        }
        else {
                $html .=  '<div class="registrationblock"><input class="btn btn-dark" type="submit" name="sendregistration" value="Verstuur bericht"><div>';
        }
        $html .=  '</form>';

        //If the creation of the account as gone wrong, the error message will be generated.
        if(isset($error)){
            if(isset($currentform['sendregistration']) && isset($error)){
                $html = $html.'<br><h2 class="errormessage">'.$error.'<h2>';
            }
        }
        
        return $html;  
    }
    

    /* ===============================================================
    ======== Author:        Mark de Bruin
    ======== Decription:    This is used to validate formfields. if will be run for each POST/GET key
    ======== How:           1.  It will check if the value is not empty if so, step x
    ========                    1.1.    It will check for specific fields that there are no special characters used. 
    ========                    1.2.    It will check if the email contains an @ sign
    ========                    1.3.    It will check if the postalcode has the following "0000 AA"
    ========                2.  It will check if the boxes have a value (except the insertion)
    ======== */
    function generateFormErrormessage($formpart,$value)
    {
        // It will check if the value is not empty if so, step x
        if(!empty($value))
        {
            // It will check for specific fields that there are no special characters used. 
            if(($formpart == 'firstname' || $formpart == 'insertion' || $formpart == 'lastname' || 
                $formpart == 'street' || $formpart == 'city' || $formpart == 'name') 
                && preg_match('/[^a-z0-9 _]+$/i', $value))
            {
                $returnmessage = 'Speciale tekens gebruikt';
            }
            // It will check if the email contains an @ sign
            elseif($formpart == 'email' && !preg_match('/@/', $value))
            {
                $returnmessage = 'Geen @ gebruikt';
            }
            // It will check if the postalcode has the following "0000 AA"
            elseif($formpart == 'postalcode' && !preg_match('~\A[0-9]\d{3} ?[a-zA-Z]{2}\z~', str_replace(' ', '', $value)))
            {
                $returnmessage = 'Incorrect. Voorbeeld: 0000AA';
            }
            // No issues
            else
            {
                $returnmessage = null;
            }
        }
        else
        {
        if($formpart <> 'insertion')
        {
            $returnmessage = 'Mag niet leeg zijn';
        }
        //It will check if the boxes have a value (except the insertion)
        else
        {
            $returnmessage = null;
        }
    }
    return $returnmessage;
}
?>