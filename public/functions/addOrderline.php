<?php

    /* ===============================================================
    ======== Decription:    This function creates a row in the orderlines table
    ========                
    ========                
    ======== How:           1.      The new orderlineID will be retrieved from the database
    ========                2.      The insert statement will be created
    ========                3.      The insert statement will be executed
    ========                4.      If there is an error, a notification will be generated.
    ======== */

    function addOrderLine($orderID, $productID, $product){
        // The new orderlineID will be retrieved from the database
        $newOrderlineID= Select("select (max(orderlineid)+1) as NEWORDERLINEID from orderlines")[0];       
        // The insert statement will be created
        $sql_OrderLineInsert =  "INSERT INTO WIDEWORLDIMPORTERS.ORDERLINES (OrderLineID, OrderID, StockItemID, Description, PackageTypeID, Quantity, 
                                UnitPrice, TaxRate, PickedQuantity, PickingCompletedWhen, LastEditedBy, LastEditedWhen
                                )
                                VALUES
                                (".
                                /*OrderLineID*/ $newOrderlineID['NEWORDERLINEID'].", ".
                                /*OrderID*/ $orderID.", ".
                                /*StockItemID*/ $productID.", ".
                                /*Description*/ "'orderline'".", ".
                                /*PackageTypeID*/ "7, ".
                                /*Quantity*/ $product['amount'].", ".
                                /*UnitPrice*/ $product['unitprice'].", ". //EXCL. BTW
                                /*TaxRate*/ "0.21, ".
                                /*PickedQuantity*/ $product['amount'].", ".
                                /*PickingCompletedWhen*/ "now(), ".
                                /*LastEditedBy*/ "3262, ".
                                /*LastEditedWhen*/ "now()".
                                ")";

        // The insert statement will be executed.  If there is an error, a notification will be generated.               
        if($res = updateDeleteInsert($sql_OrderLineInsert))
        { 
            setNotification("Er is een fout ontstaan. <br>".$res, "true");
            return;
        }
    }
        

?>