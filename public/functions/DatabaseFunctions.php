<?php
    define('HOST', "localhost");
    define('USER', "root");
    define('PASS', "");
    define('DATABASE', "wideworldimporters");

    // Tries to connect to the database using the variables determined above
    // If no connection can be made a error is shown
    function startConnetion(){
        $connection = mysqli_connect(HOST, USER, PASS, DATABASE);
        if(!$connection) throw new Exception("Unable to connect to MySQL: ");
        return $connection;
    }

    // Closes the connection to the database
    function closeConnection($connection) {
        mysqli_close($connection);
    }

    // Used to retrieve data from the database using sql code
    function Select($sql) {
        $connection = startConnetion();
        $result = mysqli_fetch_all(mysqli_query($connection, $sql),MYSQLI_ASSOC);
        closeConnection($connection);
        return $result;
    }

    // Used to add or delete data to or from de database using sql code
    // If no connection can be made a error is shown
    function updateDeleteInsert($sql) {
        $connection = startConnetion();
        
        if (mysqli_query($connection, $sql)) {
            $result = false;
         } 
         else {
            $result = mysqli_error($connection);
         }
         closeConnection($connection);
         return $result;
    }
?>