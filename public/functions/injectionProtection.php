<?php
    function injectionProtection ($string){
        $replacements = ["'", ";", '"', "`", "DELETE", "INSERT", "SELECT", "(", ")"];

        foreach($replacements as $replacement){
            $string = str_replace($replacement, "", $string);
        }
       
        return $string;
    }
?>