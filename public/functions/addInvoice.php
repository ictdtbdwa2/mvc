<?php 
    /* ===============================================================
    ======== Decription:    This function creates a row in the invoices table
    ========                
    ========
    ======== How:           1.      The total product amount is calculated
    ========                2.      The orderID will be retrieved after creating the order
    ========                3.      The New InvoiceID will be retrieved.
    ========                4.      The insert statement will be created
    ========                5.      The insert statement will be executed
    ========                6.      For each product in the shopping cart: a new orderline and invoiceline will be created
    ========                7.      If there is an error, a notification will be generated
    ======== */

    function addInvoice(){
        // The variable $totalProductAmount is set to 0
        $totalProductAmount = 0;
        // The variable $products will be filled with the values from $_SESSION['shopping_cart']
        $products = $_SESSION['shopping_cart'];     
        // The variable $customerID will be filled with the values from $_SESSION['user_id']
        $customerID = $_SESSION['user_id'];
        // The variable $settings will be filled with the values from $_SESSION['settings']
        $settings = $_SESSION['settings'];
        // The variable $date will be filled with the values from $_SESSION['date']
        $date = $settings['date'];
        // The variable $deliveryAddressID will be filled with the values from $_SESSION['deliveryAddressID']
        $deliveryAddressID = $settings['deliveradress'];
        // The variable $isGift is set to 1.99 (if the value $settings['isGift'] is set)
        $isGift = isset($settings['isGift']) ? "1.99" : "null";

        // The total productamount will be calculated
        foreach($products as $product){
            $totalProductAmount += $product['amount'];
        }
        
        // By creating the order the new order is will be set 
        $orderID = addOrder($date, $customerID);

        // retrieving the new invoiceID
        $newInvoiceID = select("select (max(invoiceid)+1) as NEWINVOICEID from invoices")[0];
        $sql_InvoiceInsert =    "INSERT INTO WIDEWORLDIMPORTERS.INVOICES(InvoiceID, CustomerID, BillToCustomerID, OrderID, DeliveryMethodID, ContactPersonID, AccountsPersonID, 
                                SalespersonPersonID, PackedByPersonID, InvoiceDate, CustomerPurchaseOrderNumber, IsCreditNote, CreditNoteReason, Comments, DeliveryInstructions,
                                InternalComments, TotalDryItems, TotalChillerItems, DeliveryRun, RunPosition, ReturnedDeliveryData, ConfirmedDeliveryTime, ConfirmedReceivedBy,
                                ChocolyDeliveryaddress, LastEditedBy, LastEditedWhen, isGift)
                                VALUES
                                (".
                                /*InvoiceID*/ $newInvoiceID['NEWINVOICEID'].", ".
                                /*CustomerID*/ $customerID.", ".
                                /*BillToCustomerID*/ $customerID.", ".
                                /*OrderID*/ $orderID.", ". 
                                /*DeliveryMethodID*/ "5, ".
                                /*ContactPersonID*/ "3262, ".
                                /*AccountsPersonID*/ "3262, ".
                                /*SalespersonPersonID*/ "3262, ".
                                /*PackedByPersonID*/ "3262, ".
                                /*InvoiceDate*/ "now(), ".
                                /*CustomerPurchaseOrderNumber*/ "null, ".
                                /*IsCreditNote*/ "0, ".
                                /*CreditNoteReason*/ "null, ".
                                /*Comments*/ "null, ".
                                /*DeliveryInstructions*/ "null, ".
                                /*InternalComments*/ "null, ".
                                /*TotalDryItems*/ "0, ".
                                /*TotalChillerItems*/ $totalProductAmount.", ". 
                                /*DeliveryRun*/ "null, ".
                                /*RunPosition*/ "null, ".
                                /*ReturnedDeliveryData*/ "null, ".
                                /*ConfirmedDeliveryTime*/ "null, ".
                                /*ConfirmedReceivedBy*/ "null, ".
                                /*ChocolyDeliveryaddress*/ $deliveryAddressID.", ".
                                /*LastEditedBy*/ "3262, ".
                                /*LastEditedWhen*/ "now(),".
                                /*isGift*/ $isGift.
                                ")";  

        // The insert statement will be executed.  If there is an error, a notification will be generated.             
        if($res = updateDeleteInsert($sql_InvoiceInsert))
        { 
            setNotification("Er is een fout ontstaan. <br>".$res, "true");
            return;
        }
        
        // For each product in the shopping cart: a new orderline and invoiceline will be created
        foreach ($products as $productID => $product){
            
            addOrderLine($orderID, $productID, $product);
            
            addInvoiceline($newInvoiceID['NEWINVOICEID'], $productID, $product);
            
        }
        // shopping card wll be truncated
        unset($_SESSION['shopping_cart']);
    }
?>