<?php
    /* ===============================================================
    ======== Decription:    This function will create a new address in the Deliveryaddresses table (if necessary).
    ========                After that it will create a link in the table CustomerDeliveryaddresses table
    ========                                
    ======== How:           1.  $address will be filled with a select statement to see if the address is already 
                                known in the Deliveryaddresses table.
    ========                2.  The select statement will be executed and the result (if any) will be stored in 
                                the variable $addressAlreadyExists
    ========                3.  If the result is null the new address will be created. If not it will skip this step.  
    ========                    3.1.    The insert statement will be created
    ========                    3.2.    The insert statement will be executed
    ========                    3.3.    The used DeliveryaddressID will be retrieved
    ========                4.  The insert statement will be created
    ========                5.  The insert statement will be executed
    ========                5.  If there is an issue with the execution, a notification will be generated.    
    ======== */

    function addAddress ($newCustomerID, $userInformation, $default = "1") {
       
        //Select statement to retrieve the used DeliveryaddressesID
        $address =  "select DeliveryaddressesID from deliveryaddresses 
        where usagetype = 'Chocoly' and lower(city) ='".strtolower($userInformation['city'])."' and lower(replace(postalcode, ' ', '')) = '".strtolower(str_replace(' ', '', $userInformation['postalcode']))."'".
        " and lower(addressline1)='".strtolower($userInformation['street'])." ".strtolower($userInformation['housenumber'])."'";

        //Retrieves the used DeliveryaddressesID
        $addressAlreadyExists = Select($address);
        
        if(empty($addressAlreadyExists)){
            // Used as the insert statement for the deliveryaddresses table
            $sql_addressinsert =   "INSERT INTO  deliveryaddresses(
                city, postalcode, addressline1, LastEditedBy, ValidFrom, ValidTo, UsageType
                )
                VALUES
                (".
                /*city*/ "'".ucfirst(strtolower($userInformation['city']))."', ".
                /*postalcode*/ "replace(upper('".$userInformation['postalcode']."'), ' ',''), ".
                /*addressline1*/ "'".ucfirst(strtolower($userInformation['street']))." ".$userInformation['housenumber']."', ".
                /*LastEditedBy*/ "3262, ".
                /*ValidFrom*/ "now(), ".
                /*ValidTo*/ "'9999-12-31 23:59:59.0', ".
                /*UsageType*/ "'Chocoly'".
                ")";

            // Inserting the new address row and immediately checks if it went succesfull
            if($res = updateDeleteInsert($sql_addressinsert)){ 
                setNotification("Toevoegen van dit account is mislukt. <br>".$res, "true");
                return;
            }

            $addressAlreadyExists = Select($address);

        }
        
        // Used as the insert statement for the customerdeliveryaddresses table
        $sql_addresslinktableinsert =   "INSERT INTO  customerdeliveryaddresses(
                                        CustomerID, DeliveryaddressesID, DeliveryaddressDefault, InvoiceAddressDefault, LastEditedBy, ValidFrom, ValidTo, UsageType
                                        )
                                        VALUES
                                        (".
                                        /*CustomerID*/ $newCustomerID.", ".
                                        /*DeliveryaddressesID*/ $addressAlreadyExists[0]['DeliveryaddressesID'].", ".
                                        /*DeliveryaddressDefault*/ $default.", ".
                                        /*InvoiceAddressDefault*/ $default.", ".
                                        /*LastEditedBy*/ "3262, ".
                                        /*ValidFrom*/ "now(), ".
                                        /*ValidTo*/ "'9999-12-31 23:59:59.0', ".
                                        /*UsageType*/ "'Chocoly'".
                                        ")";
        // Inserting the new addresslink row and immediately checks if it went succesfull
        if($res = updateDeleteInsert($sql_addresslinktableinsert)){ 
            setNotification("Toevoegen van dit account is mislukt.<br>".$res, "true");
            return;
        }

        return $addressAlreadyExists[0]['DeliveryaddressesID'];
    }
?>