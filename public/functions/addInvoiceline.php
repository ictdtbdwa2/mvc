<?php
    /* ===============================================================
    ======== Decription:    This function creates a row in the invoicelines table
    ========                
    ========
    ======== How:           1.      The new InvoicelineID will be retrieved from the database
    ========                2.      The Taxamount will be generated.
    ========                3.      The extended (total)price will generated.
    ========                4.      The insert statement will be created
    ========                5.      The insert statement will be executed
    ========                6.      If there is an error, a notification will be generated.
    ======== */
    function addInvoiceline ($invoiceID, $productID, $product) {
        // The Taxamount will be generated.
        $taxAmount = 0.21 * $product['amount'];
        // The extended (total)price will generated.
        $extendedPrice = $product['unitprice'] * $product['amount'];
        
        $newInvoicelineID = Select("select (max(invoicelineid)+1) as NEWINVOICELINEID from invoicelines")[0];     
        $sql_InvoiceLineInsert =    "INSERT INTO WIDEWORLDIMPORTERS.INVOICELINES (InvoiceLineID, InvoiceID, StockItemID, Description, PackageTypeID, 
                                    Quantity, UnitPrice, TaxRate, TaxAmount, LineProfit, ExtendedPrice, LastEditedBy, LastEditedWhen
                                    )
                                    VALUES
                                    (".
                                    /*InvoiceLineID*/ $newInvoicelineID['NEWINVOICELINEID'].", ".
                                    /*InvoiceID*/ $invoiceID.", ". 
                                    /*StockItemID*/ $productID.", ". 
                                    /*Description*/ "'".substr($product['stockitemname'],0,100)."'".", ". 
                                    /*PackageTypeID*/ "7, ".
                                    /*Quantity*/ $product['amount'].", ". 
                                    /*UnitPrice*/ $product['unitprice'].", ". //EXCL. BTW
                                    /*TaxRate*/ "0.21, ".
                                    /*TaxAmount*/ $taxAmount.", ".
                                    /*LineProfit*/ "0, ".
                                    /*ExtendedPrice*/ $extendedPrice.", ".
                                    /*LastEditedBy*/ "3262, ".
                                    /*LastEditedWhen*/ "now()".
                                    ")";    

        // The insert statement will be executed.  If there is an error, a notification will be generated.
        if($res = updateDeleteInsert($sql_InvoiceLineInsert))
        { 
            setNotification("Er is een fout ontstaan. <br>".$res, "true");
            return;
        }
    }
?>