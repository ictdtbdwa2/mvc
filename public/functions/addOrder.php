<?php
    /* ===============================================================
    ======== Decription:    This function creates a row in the orders table
    ========                
    ========
    ======== How:           1.      The new orderID will be retrieved from the database
    ========                2.      The insert statement will be created
    ========                3.      The insert statement will be executed
    ========                4.      If there is an error, a notification will be generated.    
    ======== */
    function addOrder ($date, $customerID){
        // The new orderID will be retrieved from the database
        $newOrderID = Select("select (max(orderid)+1) as NEWORDERID from orders")[0];
        // The insert statement will be created
        $sql_OrderInsert = "INSERT INTO WIDEWORLDIMPORTERS.ORDERS (OrderID, CustomerID, SalespersonPersonID, PickedByPersonID, ContactPersonID, BackorderOrderID, OrderDate, 
                            ExpectedDeliveryDate, CustomerPurchaseOrderNumber, IsUndersupplyBackordered, Comments, DeliveryInstructions,
                            InternalComments, PickingCompletedWhen, LastEditedBy, LastEditedWhen
                            )
                            VALUES
                            (".  
                            /*OrderID*/ $newOrderID['NEWORDERID'].", ". //NEWORDERID
                            /*CustomerID*/ $customerID.", ". //CUSTOMERID
                            /*SalespersonPersonID*/ "3262, ".
                            /*PickedByPersonID*/ "NULL, ".
                            /*ContactPersonID*/ "3262, ".
                            /*BackorderOrderID*/ "NULL, ".
                            /*OrderDate*/ "now(), ".
                            /*ExpectedDeliveryDate*/ "'".$date."', ". //Aanpassen? Nu heb ik een verwachte leverdatum van 4 dagen aangehouden?
                            /*CustomerPurchaseOrderNumber*/ "NULL, ".
                            /*IsUndersupplyBackordered*/ "1, ".
                            /*Comments*/ "NULL, ".
                            /*DeliveryInstructions*/ "NULL, ".
                            /*InternalComments*/ "NULL, ".
                            /*PickingCompletedWhen*/ "NULL, ".
                            /*LastEditedBy*/ "3262, ".
                            /*LastEditedWhen*/ "now()".
                            ")";


        // The insert statement will be executed.  If there is an error, a notification will be generated.
        if($res = updateDeleteInsert($sql_OrderInsert))
        { 
            setNotification("Er is een fout ontstaan. <br>".$res, "true");
            return;
        }

        return $newOrderID['NEWORDERID'];
    }
    
?>