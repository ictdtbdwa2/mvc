<?php
    $script = '';
    $_SESSION['script'] = '';
    // Shows notifications in a popup
    function setNotification ($message, $error = "false") {
        if(!empty($message)){
            $_SESSION['script'] .= '
                <script>
                    notification("'.$message.'", '.$error.');
                </script>
            ';
        }
        
        return $script;
    }
?>