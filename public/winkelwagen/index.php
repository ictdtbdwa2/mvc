<?php 
    include_once('../core.php');

    if (isset($_POST['delete'])){
        $products = removeFromCart($_POST['delete']);
    }

    if (isset($_POST['change'])){
        $products = changeFromCart($_POST['change']);
    }

    $tp = 0;
    $producthtml = '';

    // Sets all the products in the shoppingbasket or shows a message if the shoppingbasket is empty
    if($products){
        foreach($products as $product){
            $tpex += number_format($product['unitprice'] * $product['amount'], 2);
            $tp += number_format(number_format($product['unitprice'] * 1.21, 2) * $product['amount'], 2);
            $producthtml .= '
            <div class="product cart-product">
                <div class="row">
                    <div class="col-2">
                        <img src="'.$product['defaultmedia'].'" alt="" srcset="">
                    </div>
                    <div class="col-10">
                        <div class="row">
                            <div class="col-12">
                                <h4 class="header">'.$product['stockitemname'].'</h4>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-4">
                                <form action="" method="post">
                                    <input type="hidden" name="delete" value="'.$product['stockitemid'].'">
                                    <button class="btn-icon"><i class="fas fa-trash"></i></button>
                                </form>
                            </div>
                            <div class="col-6 text-right">
                                <form action="" method="post">
                                    <input type="hidden" name="change" value="'.$product['stockitemid'].'">
                                    <input class="text-center" name="amount" onchange="this.form.submit()" type="number" min="1" value="'.$product['amount'].'">
                                </form>
                            </div>
                            <div class="col-2">
                                <h3>€ '.number_format($product['unitprice'] * 1.21, 2).'</h3>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
            ';
        }
    }
    else {
        $producthtml = "<h4 class='text-center'>Winkelwagen is leeg.</h4>";
    }
?>
<!DOCTYPE html>
<html lang="nl">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Chocoly</title>
    <link href="<?=$link?>images/favicon.svg" rel="icon" >
    <link href="<?=$link?>css/main.css" rel="stylesheet">
    <script src="<?=$link?>js/jquery.min.js"></script>
</head>
<body>
<?php include($link."menu.php"); ?>
    <div id="winkelwagen" class="container">
        <div class="row">
            <div class="col-6">
                <?php
                    echo $producthtml;
                ?>
            </div>
            <div class="col-2"></div>
            <div class="col-4">
                <div class="bg-info cart-price section-small">
                    <div class="section-small">
                        <b class="text-white w-50">Totale prijs excl btw:</b><p class="text-white w-50 text-right">€ <?=number_format($tpex, 2);?></p>
                    </div>
                    <div class="section-small">
                        <b class="text-white w-50">BTW:</b><p class="text-white w-50 text-right">21%</p>
                    </div>
                    <div class="divider-top">
                        <b class="text-white w-50">Totale prijs:</b><p class="text-white w-50 text-right">€ <?=number_format($tp, 2)?></p>
                    </div>
                </div>
                <div>
                    <?php
                        // If the user isn't logged in a message and a login button are shown
                        // If the user is logged and there are items in the shoppingcart the order button is shown
                        // Otherwise a non interactible button is shown
                        if(empty($_SESSION['user_id'])) {
                            echo '
                                <div class="text-danger section-small">Om te kunnen bestellen, moet je ingelogd zijn.</div>
                                <button onclick="loginPopup()" class="btn btn-dark">Inloggen</button>
                                <a href="/registratie" class="btn btn-dark">Klant worden</a>
                            ';
                        }
                        elseif(!empty($_SESSION['shopping_cart'])){
                            echo '<a href="/bestellen" class="btn btn-dark">Bestellen</a>';
                        }
                        else {
                            echo '<button disabled="true" class="btn btn-dark">Bestellen</button>';
                        }
                    ?>
                </div>
            </div>
        </div>
    </div>
    <?php include_once($link.'footer.php');?>
</body>
</html>
