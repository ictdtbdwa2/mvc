<?php
    include_once('../core.php');

    $profile = Select("select * from wideworldimporters.v_Choc_profilepage where customerid = ".$_SESSION["user_id"]." limit 1")[0];
    $deliverAddresses = Select("select * from wideworldimporters.v_Choc_customerdeliveryaddresses where customerid = ".$_SESSION["user_id"]);
    $deliverhtml = '';
    
    // Gets and places all the delivery addresses
    foreach($deliverAddresses as $deliverAddress) {
        $deliverhtml .= '
        <div class="col-3">
            <div class="deliver-address section-small">
                <p>'.$profile["customername"].'<br>
                '.$deliverAddress['addressline1'].'<br>
                '.$deliverAddress['postalcode'].' '.strtoupper($deliverAddress['city']).'</p>
            </div>
        </div>';
    }

    $link = '../';
?>

<!DOCTYPE html>
<html lang="nl">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Chocoly</title>
    <link href="<?=$link?>images/favicon.svg" rel="icon" >
    <link href="<?=$link?>css/main.css" rel="stylesheet">
    <script src="<?=$link?>js/jquery.min.js"></script>
</head>
<body>
    <?php include($link."menu.php"); ?>
    <div id="profile" class='container'>
        <div class="row">
            <div class="col-4">
                <div class="btn-menu">
                    <a class='btn btn-info w-50 text-left'>
                        <h5 class="text-white">Profiel</h5>
                    </a>
                    <a href="/privacyinformatie" class='btn btn-dark w-50 text-left'>
                        <h5>Privacy informatie</h5>
                    </a>
                    <a href="/bestellingen" class='btn btn-dark w-50 text-left'>
                        <h5>Bestellingen</h5>
                    </a>
                </div>
            </div>
            <div class="col-8">
                <div class="row">
                    <div class="col-12">
                        <h2 class="header" >Profiel</h2>
                    </div>
                    <div class="col-12">
                        <div class='grid'>
                            <div class="column"><label>Naam</label></div>
                            <div class="column"><?= $profile["customername"] ?></div>
                            <div class="column"><label>Emailadres</label></div>
                            <div class="column"><?= $profile["emailaddress"] ?></div>
                            <div class="column"><label>Adres</label></div>
                            <div class="column"><?= $profile["addressline1"] ?> </br> <?= $profile["postalcode"] ?></div>
                            <div class="column"><label>Nieuwsbrief</label></div>
                            <div class="column"><?= $profile["chocolynewsletter"] == false || $profile["chocolynewsletter"] == null ? "Niet ingeschreven" : "Ingeschreven" ?></div>
                            <div class="column"><label>Bezorgadressen</label></div>
                        </div>
                    </div>
                    <?= $deliverhtml ?>
                    <div class="col-12">
                        <a href="bewerken" class='btn btn-dark w-25'>Bewerken</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php include_once($link.'footer.php'); isAuth();?>
</body>
</html>