<?php
    include_once('../../core.php');

    // Updates the user information
    if(isset($_POST['updateUser'])){
        $userinformation = $_POST;
        updateUser($userinformation);
    }

    $profile = Select("select * from wideworldimporters.v_Choc_profilepage where customerid = ".$_SESSION["user_id"]." limit 1")[0];
?>

<!DOCTYPE html>
<html lang="nl">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Chocoly</title>
    <link href="<?=$link?>images/favicon.svg" rel="icon" >
    <link href="<?=$link?>css/main.css" rel="stylesheet">
    <script src="<?=$link?>js/jquery.min.js"></script>
</head>
<body>
    <?php include($link."menu.php"); ?>
    <div id="profile" class='container'>
        <div class="row">
            <div class="col-4">
                <div class="btn-menu">
                    <a href="../" class='btn btn-info w-50 text-left'>
                        <h5 class="text-white">Terug</h5>
                    </a>
                </div>
            </div>
            <div class="col-8">
                <form action="" method="post">
                    <h4>Profiel</h4>
                    <br>
                    <div class='grid'>
                        <div class="column"><label>Naam</label></div>
                        <div class="column">
                            <input class='w-100' name="customerName" value="<?=$profile['customername']?>" type="text">
                        </div>
                        <div class="column"><label>Emailadres</label></div>
                        <div class="column">
                            <input class='w-100' name="emailaddress" value="<?=$profile['emailaddress']?>" type="email">
                        </div>
                        <div class="column"><label>Nieuwsbrief</label></div>
                        <div class="column">
                            <input type="checkbox" name='newsletter' <?=isset($profile['chocolynewsletter']) ? "checked" : "" ?> type="text">
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="col-12">
                            <input type="submit" class='btn btn-dark w-25 mt-4' value="Opslaan" name="updateUser">
                        </div>
                    </div>
                </form>
                
            </div>
        </div>
    </div>
    <?php include_once($link.'footer.php'); isAuth();?>
</body>
</html>
